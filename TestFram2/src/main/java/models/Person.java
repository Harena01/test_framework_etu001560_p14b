package models;

import annotation.Annotation;
import modelView.ModelView;

import java.util.Date;
import java.util.HashMap;

public class Person {
    private int id;
    private String name;
    private Date dateNaissance;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Annotation(name= "person/save")
    public ModelView save(){
        ModelView modelView = new ModelView();
        HashMap<Object, Object> data = new HashMap<>();
        data.put("person", this);
        modelView.setUrl("/home.jsp");
        modelView.setData(data);
        return modelView;
    }
}
