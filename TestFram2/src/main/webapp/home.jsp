<%@ page import="models.Person" %><%--
  Created by IntelliJ IDEA.
  Date: 16/11/2022
  Time: 15:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<% Person person = (Person) request.getAttribute("person");

%>

<html>
<head>
    <title>Test</title>
</head>
<body>
    <p>Name : <%out.println(person.getName());%></p>
    <p>Date naissance : <%out.println(person.getDateNaissance());%></p>
    <p>Id : <%out.println(person.getId());%></p>
</body>
</html>
